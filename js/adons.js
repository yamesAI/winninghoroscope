// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

jQuery.fn.extend({
  adonHideOnScrollDown: function () {
    tags = $(this)
    position = $(window).scrollTop()
    tags.each(function (tag) {
      $(this).addClass("adonHideOnScrollDown")
      $(this).mouseover(function () {
        $(this).addClass("adonHover")
      })
      $(this).mouseout(function () {
        $(this).removeClass("adonHover")
      })
    })
    $(window).scroll(function () {
      tags.each(function (tag) {
        if ($(window).scrollTop() > position) {
          $(this)._adonHideIfNotHovering()
        } else {
          $(this).removeClass("adonHide")
        }
      })
      // reset the position after hiding all tags
      position = $(window).scrollTop()
    })
    setInterval(function () {
      curr_position = $(window).scrollTop()
      if (curr_position === position) {
        tags.each(function (tag) {
          $(this)._adonHideIfNotHovering()
        })
      }
    }, 2000)
  },
  _adonHideIfNotHovering: function () {
    if (!$(this).hasClass("adonHover")) $(this).addClass("adonHide")
  },
})

$(document).ready(function () {
  $("[href]").each(function () {
    if ($(this).href == window.location.href) {
      $(this).addClass("active")
    }
  })
})

// NB: Twinned with /stylesheets/adon/adonNavScrollTo.js
// NB: Tested by /test/adon/adonNavScrollTo.js

String.prototype.slugify = function () {
  return this.toLowerCase()
    .trim()
    .replace(/\ /, "-")
    .match(/[a-z0-9\-]/g)
    .join("")
}

jQuery.fn.extend({
  adonNavScrollId: function () {
    return $(this).each(function () {
      TARGET = $(this)
      targetText = TARGET.text().slugify()
      targetTop = TARGET.offset().top
      TARGET.attr("id", targetText)
      $("a")._adonActivateLink(targetText, targetTop)
      return TARGET
    })
  },
  adonNavScrollLightup: function () {
    return $(this).each(function () {
      TARGET = $(this)
      targetText = TARGET.text().slugify()
      targetTop = TARGET.offset().top
      TARGET.attr("id", targetText)
      $("a")._adonActivateLightUp(targetText, targetTop)
      return TARGET
    })
  },
  _adonActivateLink: function (targetText, targetTop) {
    return $(this).each(function () {
      TAG = $(this)
      if (targetText === TAG.text().slugify() && !TAG.attr("href")) {
        TAG.parent("li").addClass("adonNavScrollLink")
        TAG.click(function () {
          $("html, body").animate(
            {
              scrollTop: targetTop,
            },
            1000
          )
        })
      }
      return TAG
    })
  },
  _adonActivateLightUp: function (targetText, targetTop) {
    return $(this).each(function () {
      TAG = $(this)
      if (targetText === TAG.text().slugify() && !TAG.attr("href")) {
        tO = window.setTimeout(TAG._adonMyTestScrollPosition(targetTop), 1000)
        $(window).scroll(function () {
          window.clearTimeout(tO)
          tO = window.setTimeout(TAG._adonMyTestScrollPosition(targetTop), 1000)
        })
      }
      return TAG
    })
  },
  _adonMyTestScrollPosition: function (targetTop) {
    TAG = $(this)
    winY = $(window).scrollTop()
    if (winY - 20 <= targetTop && winY + 20 >= targetTop) {
      TAG._adonLightup()
    }
    if (winY + 50 > document.body.scrollHeight - window.innerHeight) {
      TAG._adonLightup()
    }
  },
  _adonLightup: function () {
    TAG = $(this)
    TAG.parent("li").siblings().removeClass("adonnavactive")
    TAG.parent("li").addClass("adonnavactive")
    return TAG
  },
  _adonNavScrollToHeading: function () {
    TAG = $(this)
    return $("#" + TAG.text().slugify())
  },
})
